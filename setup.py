from setuptools import setup, find_packages
import os

package_name = 'speechai'

def get_requires():
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'requirements.txt'), 'r') as f:
        lines = f.readlines()
    requires = []
    for line in lines:
        requires.append(line)
    return requires

def get_version():
    import importlib
    m = importlib.import_module(package_name)
    return m.__version__

def get_dir(dir):
    data = []
    for root, dirs, files in os.walk(dir):
        rel_root = os.path.relpath(root, dir)
        for name in files:
            if name.endswith('.py'):
                continue
            data.append(os.path.join(rel_root, name))
    # print(data)
    return data


with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "README.md"), "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name=package_name,
    version=get_version(),
    packages=find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests",
            "log",
            "log.*",
            "*.log",
            "*.log.*"
        ]
    ),
    url='https://gitlab.com/**/sampleproject',
    author='SenseTime Edu Team',
    author_email='seneseedu@sensetime.com',
    description="Speech Recognition Lab",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license='.',
    install_requires=get_requires(),
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
    python_requires='>=3.6',
    include_package_data=True,
    package_data={"speechai": get_dir("./speechai")}
)
