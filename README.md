# python-ci-cd

Collaborate on a project based on CI/CD framework. 

When you run test, you need to do:

`python3 -m pytest`

Reference: [Insight](https://www.insight-tec.com/tech-blog/20201119_gitlab/)

